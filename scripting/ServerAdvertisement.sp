#pragma semicolon 1

#include <sourcemod>
#include <multicolors>
#include <cstrike>
#include <clientprefs>
#include <geoip>
#include <hlstatsX_adv>
#include <SteamWorks>

#define PLUGIN_URL      "https://github.com/ESK0"
#define VERSION         "2.7.1"
#define PLUGIN_AUTHOR   "ESK0 & ShaRen"

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

int g_iEnable;

char g_sTag[50];
char g_sTime[32];

KeyValues g_hMessages;

char FILE_PATH[PLATFORM_MAX_PATH];

Handle Cv_filepath = INVALID_HANDLE;
//Handle h_ServerAdvertisement;
float g_fMessageDelay;


public Plugin myinfo =
{
 name         = "Server Advertisement",
 author       = PLUGIN_AUTHOR,
 version      = VERSION,
 description  = "Server Advertisement",
 url          = PLUGIN_URL
};

public OnPluginStart()
{
  AutoExecConfig(true);
  CreateConVar("ServerAdvertisement_version", VERSION, "Server Advertisement plugin", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
  Cv_filepath = CreateConVar("ServerAdvertisement_filepath", "addons/sourcemod/configs/ServerAdvertisement.cfg","Path for file with settings");
  GetConVarString(Cv_filepath, FILE_PATH,sizeof(FILE_PATH));
  LoadConfig();
}
public OnMapStart()
{
  LoadMessages();
  CreateTimer(g_fMessageDelay, Event_PrintAdvert, _,TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}
public OnMapEnd()
{

}
public Action Event_PrintAdvert(Handle timer)
{
 PrintAdverToAll();
}
public Action PrintAdverToAll()
{
  if(g_iEnable) {
    if(!g_hMessages.GotoNextKey()) {
      g_hMessages.GoBack();
      g_hMessages.GotoFirstSubKey();
    }
    LoopClients(i) {      //GetClientCookie(i, h_ServerAdvertisement, cookievalue, sizeof(cookievalue));
      if(IsValidPlayer(i) ) {
        char sText[256], sBuffer[256], sType[12], sMinTime[12], sMaxTime[12], sIP[26];
        int  iPlTime = GetClientPlayedTime(i);
        GetClientIP(i, sIP, sizeof(sIP));

        if(SteamWorks_HasLicenseForApp(i, 730) == k_EUserHasLicenseResultHasLicense)
          KvGetString(g_hMessages, "steam", sText, sizeof(sText), "MISSING");
        else KvGetString(g_hMessages, "pirat", sText, sizeof(sText), "MISSING");

        if (StrEqual(sText, "MISSING"))
          g_hMessages.GetString("default", sText, sizeof(sText));
          
        if(StrContains(sText , "{NEXTMAP}") != -1) {
          GetNextMap(sBuffer, sizeof(sBuffer));
          ReplaceString(sText, sizeof(sText), "{NEXTMAP}", sBuffer);
        }
        if(StrContains(sText, "{CURRENTMAP}") != -1) {
          GetCurrentMap(sBuffer, sizeof(sBuffer));
          ReplaceString(sText, sizeof(sText), "{CURRENTMAP}", sBuffer);
        }
        if(StrContains(sText, "{SERVERIP}") != -1) {
          int ips[4],
              ip = GetConVarInt(FindConVar("hostip")),
              port = GetConVarInt(FindConVar("hostport"));
          ips[0] = (ip >> 24) & 0x000000FF;
          ips[1] = (ip >> 16) & 0x000000FF;
          ips[2] = (ip >> 8) & 0x000000FF;
          ips[3] = ip & 0x000000FF;
          Format(sBuffer, sizeof(sBuffer), "%d.%d.%d.%d:%d", ips[0], ips[1], ips[2], ips[3], port);
          ReplaceString(sText, sizeof(sText), "{SERVERIP}", sBuffer);
        }
        if(StrContains(sText , "{PLAYERNAME}") != -1) {
          Format(sBuffer, sizeof(sBuffer), "%N", i);
          ReplaceString(sText, sizeof(sText), "{PLAYERNAME}", sBuffer);
        }
        g_hMessages.GetString("mintime", sMinTime, sizeof(sMinTime), "0");
        g_hMessages.GetString("maxtime", sMaxTime, sizeof(sMaxTime), "999999999");
        if (iPlTime > StringToInt(sMinTime) && iPlTime < StringToInt(sMaxTime)) {
          g_hMessages.GetString("type", sType, sizeof(sType), "T");
          if(StrEqual(sType, "T", false))
            CPrintToChat(i,"%s %s", g_sTag, sText);
          if(StrEqual(sType, "C", false))
            PrintHintText(i,"%s", sText);
          PrintToConsole(i,"%s", sText);
        }
      }
    }
  }
}

public LoadMessages()
{
 g_hMessages = new KeyValues("ServerAdvertisement");
 if(!FileExists(FILE_PATH))
 {
   SetFailState("[ServerAdvertisement] '%s' not found!", FILE_PATH);
   return;
 }
 g_hMessages.ImportFromFile(FILE_PATH);
 if(g_hMessages.JumpToKey("Messages"))
 {
   g_hMessages.GotoFirstSubKey();
 }
}

public LoadConfig()
{
 KeyValues hConfig = new KeyValues("ServerAdvertisement");
 if(!FileExists(FILE_PATH)) {
   SetFailState("[ServerAdvertisement] '%s' not found!", FILE_PATH);
   return;
 }
 hConfig.ImportFromFile(FILE_PATH);
 if(hConfig.JumpToKey("Settings")) {
   g_iEnable = hConfig.GetNum("Enable");
   g_fMessageDelay = hConfig.GetFloat("Delay_between_messages");
   hConfig.GetString("Time_Format", g_sTime, sizeof(g_sTime));
   hConfig.GetString("Advertisement_tag", g_sTag, sizeof(g_sTag));
 } else {
   SetFailState("Config for 'Server Advertisement' not found!");
   return;
 }
 delete hConfig;
}
stock bool IsValidPlayer(int client, bool alive = false)
{
  return(1 <= client <= MaxClients && IsClientInGame(client) && !IsFakeClient(client) && (alive == false || IsPlayerAlive(client)))? true:false;
}
stock bool IsPlayerAdmin(client)
{
  return (GetAdminFlag(GetUserAdmin(client), Admin_Generic))? true:false;
}
